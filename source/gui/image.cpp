// This file is part of CaesarIA.
//
// CaesarIA is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// CaesarIA is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with CaesarIA.  If not, see <http://www.gnu.org/licenses/>.
//
// Copyright 2012-2014 Dalerank, dalerankn8@gmail.com

#include "image.hpp"
#include "gfx/engine.hpp"
#include "gfx/decorator.hpp"
#include "gfx/pictureconverter.hpp"
#include "core/color.hpp"

using namespace std;
using namespace gfx;

namespace gui
{

class Image::Impl
{
public:
  Picture bgPicture;
  PictureRef background;
  Image::Mode mode;
  bool needUpdateTexture;

  ~Impl()
  {
    background.reset();
  }

public oc3_signals:
	Signal0<> onClickedSignal;
};

//! constructor
Image::Image( Widget* parent ) : Widget( parent, -1, Rect( 0, 0, 1, 1) ), _d( new Impl )
{
	_d->mode = Image::fit;
}

Image::Image(Widget* parent, Rect rectangle, const Picture& pic, Mode mode, int id)
: Widget( parent, id, rectangle),
	_d( new Impl )
{
  if( mode == Image::image )
  {
    mode = Image::fit;
    setWidth( pic.width() );
    setHeight( pic.height() );
  }

  _d->bgPicture = pic;
  _d->needUpdateTexture = true;
  #ifdef _DEBUG
    setDebugName( "Image");
#endif
}

Image::Image(Widget* parent, Point pos, const Picture& pic, int id)
	: Widget( parent, id, Rect( pos, pic.size() ) ),
		_d( new Impl )
{
  _d->mode = Image::image;
  _d->bgPicture = pic;
  _d->needUpdateTexture = true;
}

void Image::_updateTexture(gfx::Engine& painter )
{
  Size imageSize = size();

  if( _d->background && _d->background->size() != imageSize )
  {
    _d->background.reset();  
  }

  if( !_d->background )
  {
    _d->background.reset( Picture::create( imageSize ) );
  }

  // draw button background
  if( _d->bgPicture.isValid() )
  {    
    _d->background->fill( 0x0000000, Rect() );
    switch( _d->mode )
    {
    case Image::native: _d->background->draw( _d->bgPicture, Point( 0, 0 ), true ); break;

    case Image::fit:
      _d->background->draw( _d->bgPicture, Point( width() - _d->bgPicture.width(),
                                                  height() - _d->bgPicture.height() ) / 2, false );
    break;

    case Image::image:
      _d->background->draw( _d->bgPicture,
                            Rect( Point(0, 0), _d->bgPicture.size()),
                            Rect( Point( 0, 0 ), size() ), false );
    break;
    }
  }    
  else
  {
    _d->background->fill( 0xff000000, Rect() );
  }
}

//! destructor
Image::~Image() {}

//! draws the element and its children
void Image::draw(gfx::Engine& painter )
{
  if ( !isVisible() )
    return;

  // draw background
  if( _d->background )
  {
    painter.draw( *_d->background, screenLeft(), screenTop(), &absoluteClippingRectRef() );
  }

  Widget::draw( painter );
}


Signal0<>& Image::onClicked(){  return _d->onClickedSignal;}

void Image::beforeDraw(gfx::Engine& painter )
{
  if( _d->needUpdateTexture )
  {
    _updateTexture( painter );

    _d->needUpdateTexture = false;
  }

  Widget::beforeDraw( painter );
}

void Image::setPicture( Picture picture )
{
  _d->bgPicture = picture;
  _d->needUpdateTexture = true;
}

void Image::_resizeEvent() {  _d->needUpdateTexture = true;}

void Image::setupUI(const VariantMap& ui)
{
  Widget::setupUI( ui );

  setPicture( Picture::load( ui.get( "image" ).toString() ) );
  std::string mode = ui.get( "mode", "fit" ).toString();
  if( mode == "fit" ) { _d->mode = Image::fit; }
  else if( mode == "image" ) { _d->mode = Image::image; }
  else if( mode == "native" ) { _d->mode = Image::native; }
  else { _d->mode = Image::fit; }
}

PictureRef& Image::getPicture() {  return _d->background;}

}//end namespace gui
